#include <stdio.h>
#include <stdlib.h>

/*Data Structures*/
struct node {
        int cost;
        int city1;
        int city2;
};
typedef struct node* link;

/*Global Variables*/
link *skyGraph;
int airports;
int cities;
int sky;
int roads;

/*Prototypes*/
void createGraph();
link createNode(int city1, int city2, int cost);
void evaluate();
int compare(const void* road1, const void* road2);
int* kruskal(int cities, int edges);
void joinSets(int city1, int city2, int* rep, int* rank);
int findSet(int city, int* rep);
void connect(int city1, int city2, int* rep, int* rank);
void printOutput(int* values);
void freeMemory();

int main(int argc, char** argv){
        createGraph();
        evaluate();
        freeMemory();

        return 0;
}

/*Creating Zone - responsible to create the data Structures that will
   be used to solve the problem*/
void createGraph(){
        int nCities = 0, nAirports = 0, i = 0, nRoads = 0, city1 = 0, city2 = 0, cost = 0;

        if(scanf("%d", &nCities) <0) {
                perror("Error getting input from stdin");
        }
        cities = nCities;
        sky = cities + 1;

        if(scanf("%d", &nAirports) <0) {
                perror("Error getting input from stdin");
        }
        airports = nAirports;
        link *airportsGraph = (link*) malloc(sizeof(struct node)*airports);
        for(i=0; i < airports; i++) {
                if(scanf("%d %d", &city1, &cost) <0) {
                        perror("Error getting input from stdin");
                }
                airportsGraph[i] = createNode(city1, sky, cost);

        }

        if(scanf("%d", &nRoads) <0) {
                perror("Error getting input from stdin");
        }
        roads = nRoads;
        skyGraph = (link*) malloc(sizeof(struct node)*(airports + roads));
        for(i=0; i < roads; i++) {
                if(scanf("%d %d %d", &city1, &city2, &cost) <0) {
                        perror("Error getting input from stdin");
                }
                skyGraph[i] = createNode(city1, city2, cost);
        }
        for(i=roads; i < (roads + airports); i++) {
                skyGraph[i] = airportsGraph[i-roads];
        }
        free(airportsGraph);
}


link createNode(int city1, int city2, int cost){
        link newNode = (link) malloc(sizeof(struct node));
        newNode->cost = cost;
        newNode->city1 = city1;
        newNode->city2 = city2;

        return newNode;
}


/*Evaluate zone - responsible to evaluate the problem and to print the output*/
void evaluate(){
        int* roadsValue;
        int* airportsValue;
        roadsValue = kruskal(cities, roads);
        airportsValue = kruskal(sky, (roads+airports));
        if(roadsValue[3] == 1 ) {
                if (airportsValue[3] ==1 ) {
                        printf("Insuficiente\n");
                }
                else{
                        printOutput(airportsValue);
                }
        }
        else if(airportsValue[0] < roadsValue[0] && airportsValue[3] !=1) {
                printOutput(airportsValue);
        }
        else{
                printOutput(roadsValue);
        }
        free(roadsValue);
        free(airportsValue);
}

int compare(const void *road1, const void *road2){
        return (*(link*) road1)->cost - (*(link*) road2)->cost;
}

int* kruskal(int cities, int edges){
        int *returnValue = (int*) malloc(sizeof(int)*4); /*0-cost, 1-Airports number 2- cities numbers 3-not connected flag*/
        int *rank = (int*) malloc(sizeof(int)*cities);
        int *rep =  (int*) malloc(sizeof(int)*cities);
        int i = 0, auxRep = 0;

        for(i = 0; i < 4; i++) {
                returnValue[i] = 0;
        }
        for (i=0; i < cities; i++) {
                rank[i] = 0;
                rep[i] = i;
        }
        qsort(skyGraph, edges, sizeof(link), compare);
        for(i = 0; i < (edges); i++) {
                if(findSet((skyGraph[i]->city1)-1, rep) != findSet((skyGraph[i]->city2)-1, rep)) {
                        joinSets((skyGraph[i]->city1)-1, (skyGraph[i]->city2)-1, rep, rank);
                        returnValue[0]+=skyGraph[i]->cost;
                        if((skyGraph[i]->city2) == sky) {
                                returnValue[1]++;
                        }
                        else{
                                returnValue[2]++;
                        }
                }
        }
        auxRep = findSet(0,rep);
        for(i=1; i<cities; i++) {
                if(auxRep != findSet(i,rep)){
                        returnValue[3] = 1;
                }
        }
        free(rank);
        free(rep);
        return returnValue;
}

int findSet(int city, int* rep){
        if(city != rep[city]) {
                rep[city] = findSet(rep[city], rep);
        }
        return rep[city];
}

void joinSets(int city1, int city2, int* rep, int* rank){
        connect(findSet(city1, rep), findSet(city2, rep), rep, rank);
}

void connect(int city1, int city2, int* rep, int *rank){
        if (rank[city1] > rank[city2]) {
                rep[city2] = city1;
        }
        else{
                rep[city1] = city2;
                if(rank[city1] == rank[city2]) {
                        rank[city2]++;
                }

        }
}

void printOutput(int* values){
        printf("%d\n%d %d\n", values[0], values[1], values[2]);
}

/*Free Memory zone*/
void freeMemory(){
        int i = 0;

        for(i=0; i < (roads + airports); i++) {
                free(skyGraph[i]);
        }
        free(skyGraph);
}
